package ru.t1.kotchenko.tm.repository;

import ru.t1.kotchenko.tm.api.ICommandRepository;
import ru.t1.kotchenko.tm.constant.ArgumentConstant;
import ru.t1.kotchenko.tm.constant.TerminalConstant;
import ru.t1.kotchenko.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.HELP,
            "show application commands."
    );

    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.INFO,
            "show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.VERSION,
            " show application version."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, ArgumentConstant.COMMANDS,
            " show application commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS,
            " show application arguments."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null,
            " close application."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, INFO, VERSION, COMMANDS, ARGUMENTS, EXIT
    };

    @Override
    public Command[] getTerminalCommand() {
        return TERMINAL_COMMANDS;
    }

}
