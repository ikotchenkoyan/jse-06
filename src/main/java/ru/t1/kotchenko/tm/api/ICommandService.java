package ru.t1.kotchenko.tm.api;

import ru.t1.kotchenko.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommand();

}
