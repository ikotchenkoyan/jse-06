package ru.t1.kotchenko.tm.api;

public interface IBootstrap {

    void run(String... args);

}
